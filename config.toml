baseurl = "https://user2022.r-project.org/"
title = "useR! 2022"
theme = ["hugo-user-theme","hugo-universal-theme"]
languageCode = "en-us"
# Site language. Available translations in the theme's `/i18n` directory.
defaultContentLanguage = "en"
# Enable comments by entering your Disqus shortname
#disqusShortname = "devcows"
# Enable Google Analytics by entering your tracking code
#googleAnalytics = ""

# Define the number of posts per page
paginate = 10

[markup]
  [markup.goldmark]
  [markup.goldmark.renderer]
    unsafe = true
  [markup.tableOfContents]
    endLevel = 2
    ordered = false
    startLevel = 1

[menu]

# Main menu
# Comment out sections to prevent them appearing on the nav bar
[[menu.main]]
  name = "Home"
  url  = "/"
  weight = 1

[[menu.main]]
  name = "Program"
  url  = "/program/"
  identifier = "program"
  weight = 3 

[[menu.main]]
  name = "Overview"
  parent = "program"
  url  = "/program/overview"
  weight = 1 

[[menu.main]]
  name = "Tutorials"
  parent = "program"
  url  = "/program/tutorials"
  weight = 2 

[[menu.main]]
  name = "Talks"
  parent = "program"
  url  = "/program/talks"
  weight = 3

[[menu.main]]
  name = "Posters + Elevator Pitches"
  parent = "program"
  url  = "/program/posters"
  weight = 4 

[[menu.main]]
  name = "Participate"
  url  = "/participate/"
  identifier = "participate"
  weight = 4 

[[menu.main]]
  name = "Call for Tutorials"
  parent = "participate"
  url = "/participate/call-for-tutorials"
  weight = 5 

[[menu.main]]
  name = "Call for Abstracts"
  parent = "participate"
  url = "/participate/call-for-abstracts"
  weight = 6 

[[menu.main]]
  name = "Registration"
  parent = "participate"
  url = "/participate/registration"
  weight = 1 

[[menu.main]]
  name = "Info for Presenters/Chairs"
  parent = "participate"
  url = "/participate/instructions-for-presenters"
  weight = 2 

[[menu.main]]
  name = "Sponsorship"
  parent = "participate"
  url = "/about/sponsorship"
  weight = 3 

[[menu.main]]
  name = "Accessibility"
  parent = "participate"
  url = "/participate/accessibility"
  weight = 4 

#[[menu.main]]
#  name = "Venue"
#  url  = "/venue/"
#  weight = 4

#[[menu.main]]
#  name = "News"
#  url  = "/news/"
#  weight = 5
   

[[menu.main]]
  name = "Contact"
  url  = "/contact/"
  weight = 7

#[[menu.main]]
# name = "Code of Conduct"
#  url  = "/codeofconduct/"
#  weight = 8

[[menu.main]]
  name = "about"
  url  = "/about/"
  identifier = "about"
  weight = 2 

[[menu.main]]
  name = "FAQ"
  parent = "about"
  identifier = "faq"
  url  = "/faq/"
  weight = 1 

[[menu.main]]
  name = "code of conduct"
  parent = "about"
  url = "/about/coc"
  weight = 2 

[[menu.main]]
  name = "organizers"
  parent = "about"
  url  = "/about/organizers"
  weight = 6  

[[menu.main]]
  name = "sponsors"
  parent = "about"
  url = "/about/sponsors"
  weight = 2 

[[menu.main]]
  name = "sponsorship"
  parent = "about"
  url = "/about/sponsorship"
  weight = 3 


[[menu.main]]
  name = "useR conference"
  parent = "about"
  url = "/about/user"
  weight = 5

#[[menu.main]]
#  name = "about Vanderbilt Biostatistics"
#  parent = "about"
#  url = "/about/about.vanderbilt-biostatistics"
#  weight = 9

# Top bar social links menu

[[menu.topbar]]
  weight = 1
  name = "Twitter"
  url = "https://twitter.com/_useRconf"
  pre = "<i class='fa fa-2x fa-twitter'></i>"

[[menu.topbar]]
  weight = 2
  name = "LinkedIn"
  url = "https://www.linkedin.com/company/user-conf"
  pre = "<i class='fa fa-2x fa-linkedin'></i>"

[[menu.topbar]]
  weight = 3
  name = "Email"
  url = "/contact"
  pre = "<i class='fa fa-2x fa-envelope'></i>"

[params]
  viewMorePostLink = "/news/"
  author = "useR 2022 Organizers"
  defaultKeywords = ["useR 2022", "Nashville"]
  defaultDescription = "useR 2022 website"

  # Google Maps API key (if not set will default to not passing a key.)
  #googleMapsApiKey = "AIzaSyCFhtWLJcE30xOAjcbSFi-0fnoVmQZPb1Y"

  # Style options: default (light-blue), blue, green, marsala, pink, red, turquoise, violet
  style = "default"

  # Since this template is static, the contact form uses www.formspree.io as a
  # proxy. The form makes a POST request to their servers to send the actual
  # email. Visitors can send up to a 1000 emails each month for free.
  #
  # What you need to do for the setup?
  #
  # - set your email address under 'email' below
  # - upload the generated site to your server
  # - send a dummy email yourself to confirm your account
  # - click the confirm link in the email from www.formspree.io
  # - you're done. Happy mailing!
  #
  # Enable the contact form by entering your Formspree.io email
  email = "biostatmatt@gmail.com"
  endpoint = "https://formspree.io/f/myylrped"

  about_us = """<p><strong>@_useRconf // #useR2022</strong></p>"""
  copyright = "Copyright (c) 2021 - 2022, Vanderbilt University Medical Center; all rights reserved."

  # Format dates with Go's time formatting
  date_format = "January 2, 2006"

  logo = "img/userlogo-small.png"
  logo_small = "img/userlogo-small.png"
  address = """<p><strong>Department of Biostatistics</strong>
   <br>Vanderbilt University Medical Center
      <br>2525 West End Avenue, Suite 1100
      <br>Nashville, TN 37203
      <br>
      <strong>United States</strong>
    </p>
    """
  #latitude = "-12.043333"
  #longitude = "-77.028333"

[Permalinks]
  news = "/news/:year/:month/:day/:filename/"

# Enable or disable top bar with social icons
[params.topbar]
  enable = false
  text = """<p class="hidden-sm hidden-xs">Contact us on +420 777 555 333 or hello@universal.com.</p>
    <p class="hidden-md hidden-lg"><a href="#" data-animate-hover="pulse"><i class="fa fa-phone"></i></a>
    <a href="#" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
    </p>
    """

# Enable and disable widgets for the right sidebar
[params.widgets]
  categories = true
  tags = true
  search = true
  contact_map = false

# Hero section
[params.hero]
  # To change the background image of the hero, replace 'header-bg.jpg' in
  # the 'static/img' folder.
  title = "useR! 2022  \nThe R User Conference  \nAll-virtual • Global"
  dates = "June 20-23, 2022"
  showButton = false
  buttonText = "Tell me more"

[params.about]
  enable = true 
  bg = true
  pattern = false
  photo1 = false
  photo2 = false
  title = "About"
  text = "useR! 2022 was an all-virtual conference, with 1227 registered participants from 96 countries. Recordings of most useR! 2022 presentations are available on YouTube, at https://www.youtube.com/c/useRConference_global.<br/><br/>useR! conferences are annual nonprofit gatherings organized by R community volunteers and supported by the R Foundation. Attendees include R developers and users who are data scientists, business intelligence specialists, analysts, statisticians from academia and industry, and students. The conferences showcase applications of R software and developments in the software itself, as well as new and updated R packages that provide [boundless](http://cran.r-project.org/web/packages) additional functionality to R. Community contributions form the foundation of useR! conferences.<br/><br/> If you are interested in hosting a future useR! conference, please send your proposal to conferences[at]r-project[dot]org."
  
[params.dates]
  enable = false
  bg = true
  pattern = false
  photo1 = false
  photo2 = false
  title = "Important Dates"
  # subtitle="Don't forget these!"

[params.carousel]
  enable = false
  # All carousel items are defined in their own files. You can find example items
  # at 'exampleSite/data/carousel'.
  # For more informtion take a look at the README.

[params.features]
  enable = false
  # All features are defined in their own files. You can find example items
  # at 'exampleSite/data/features'.
  # For more informtion take a look at the README.

[params.testimonials]
  enable = false
  # All testimonials are defined in their own files. You can find example items
  # at 'exampleSite/data/testimonials'.
  # For more informtion take a look at the README.
  title = "Testimonials"
  subtitle = "We have worked with many clients and we always like to hear they come out from the cooperation happy and satisfied. Have a look what our clients said about us."

[params.see_more]
  enable = false
  icon = "fa fa-file-code-o"
  title = "Do you want to see more?"
  subtitle = "We have prepared for you more than 40 different HTML pages, including 5 variations of homepage."
  link_url = "#"
  link_text = "Check other homepages"

[params.twitter]
  enable = true
  bg = false
  pattern = false
  photo1 = true
  photo2 = false
  title = "Twitter"
  feed_url = "https://twitter.com/_useRconf"

[params.contact]
  # Since this template is static, the contact form uses Netlify to capture the 
  # contact submission. The form makes a POST request to Netlify and you can set up 
  # an email notification from Netlify. You can have up to 100 submissions a form a month for free.
  #
  # What you need to do for the setup?
  #
  # You don't need to do anything for Netlify to capture the submissions.
  # In Netlify, go to the "Forms" section and click the "Settings and usage" button.
  # Scroll down to see the "Form Notifications" section, and in "Outgoing Notifications" click the 
  # "Add notification" button and click the "Email notification" option.
  # In the modal that pops up, add the official conference email address useRXXX@r-project.org.
  # In the "Form" option, select "Any form". That should be all the setup required.
  subject = [
    "Sponsorship",
    "Registration",
    "Tutorials",
    "Schedule",
    "Technical Issue",
    "Other"
  ]
  recaptcha = false

[params.sponsors]
  enable = false
  bg = false
  pattern = true
  photo1 = false
  photo2 = false
  # All clients are defined in their own files. You can find example items
  # at 'exampleSite/data/clients'.
  # For more informtion take a look at the README.
  title = "Our Sponsors"
  tier1 = "Platinum"
  tier2 = "Gold"
  tier3 = "Silver"
  tier4 = "Bronze"
  tier5 = "Small Business"


[params.recent_posts]
  enable = false
  title = "News"
  subtitle = ""

[params.participation]
  tickets_enable = false
  tickets_link = "http://www.tickets.com"
  tickets_text = "Get your ticket"
  cfp_enable = true
  cfp_link = "http://www.callforpapers.com"
  cfp_text = "Submit your paper"

[params.keynotes]
  enable = true 
  bg = true
  pattern= false
  photo1 = false
  photo2 = false
  # Possibility to center items
  center = true
  title = "Keynote Speakers"
  #subtitle = "Keynote Speakers"
  description = ""

  # All keynote speaker pictures are stored under 'static/img/keynotes'.
  # Replace them with your own too.
  # Suggestion: Image size should be at least 360px*360px or the alignment might be affected.

  [[params.keynotes.members]]
    img = "paula_moraga-400x400.jpg"
    alt = "Photo of keynote speaker Paula Moraga"
    name = "Paula Moraga"
    bio = "Paula Moraga is an assistant professor of statistics at King Abdullah University of Science and Technology (KAUST). Her research focuses on the development of statistical methods and computational tools for geospatial data analysis and health surveillance, and the impact of her work has directly informed strategic policy in reducing the burden of diseases such as malaria and cancer in several countries. Paula has worked on the development of several R packages for Bayesian risk modeling, detection of disease clusters, and risk assessment of travel-related spread of disease. She is the author of the book [Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny]( https://www.paulamoraga.com/book-geospatial/)."
    social = [
      ["fa-globe", "https://www.paulamoraga.com"],
      ["fa-twitter", "https://twitter.com/Paula_Moraga_"],
      ["fa-github", "https://github.com/Paula-Moraga"]
    ]

  [[params.keynotes.members]]
    img = "amanda-cox-400x400.jpg"
    alt = "Photo of keynote speaker Amanda Cox"
    name = "Amanda Cox"
    bio = "Amanda Cox is head of special data projects at USAFacts. She was previously the data editor of the *New York Times*. She joined its graphics department in 2005, making charts and maps for the paper and its website. In 2016, she was named the editor of The Upshot section, which offers an analytical approach to the day's news. She is a leader in the field of data visualization. Before joining the *Times*, she worked at the Federal Reserve Board and earned a master's degree in statistics from the University of Washington."
    social = [
      ["fa-twitter", "https://twitter.com/amandacox"],
    ]

  [[params.keynotes.members]]
    img = "afrimapr-400x400.png"
    alt = "Image of afrimapr logo"
    name = "afrimapr"
    bio = "The [afrimapr](https://afrimapr.github.io/afrimapr.website/) project supports the development of a community of practice in Africa around map-making in R. Project members do this by developing [learning materials](https://github.com/afrimapr/afrilearndata), [open-source components](https://wellcomeopenresearch.org/articles/5-157) and community events. The keynote will showcase a medley of experiences from project leaders and community members. afrimapr was created in 2020 by Andy South and Anelda van der Walt with initial funding from the Wellcome Trust Open Research Fund. Andy South is a data scientist based at the Liverpool School of Tropical Medicine. Anelda van der Walt is the founder of Talarify, a consulting company in South Africa, working in open science, reproducible digital and computational research, mentorship, and training.<br/><br/> "
    social = [
      ["fa-globe", "https://afrimapr.github.io/afrimapr.website/"],
      ["fa-github", "https://github.com/afrimapr/afrilearndata"],
      ["fa-twitter", "https://twitter.com/afrimapr"],
    ]

 
  [[params.keynotes.members]]
    img = "julia-silge-400x400.jpeg"
    alt = "Photo of keynote speaker Julia Silge"
    name = "Julia Silge"
    bio = "[Julia Silge](https://juliasilge.com/) is a data scientist and software engineer at RStudio PBC, where she works [on open source modeling tools](https://www.tidymodels.org/). She holds a PhD in astrophysics and has worked as a data scientist in tech and the nonprofit sector, as well as a technical advisory committee member for the US Bureau of Labor Statistics. She is an [author](https://www.tmwr.org/), an international keynote speaker, and a real-world practitioner focusing on data analysis and machine learning practice. Julia loves [text analysis](https://www.tidytextmining.com/), making beautiful charts, and communicating about technical topics with diverse audiences."
    social = [
      ["fa-youtube", "https://www.youtube.com/juliasilge"],
      ["fa-twitter", "https://twitter.com/juliasilge"],
      ["fa-github", "https://github.com/juliasilge"],
    ]

  [[params.keynotes.members]]
    img = "sebastian-meyer-400x400.jpg"
    alt = "Photo of keynote speaker Sebastian Meyer"
    name = "Sebastian Meyer"
    bio = "Sebastian Meyer is a statistician and research fellow at the Institute of Medical Informatics, Biometry and Epidemiology at Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany. He holds a PhD in epidemiology and biostatistics from the University of Zurich and maintains the R package [`surveillance`](https://surveillance.r-forge.r-project.org/pkgdown/index.html). He is an editor of the [Journal of Statistical Software](https://www.jstatsoft.org/index) and the newest member of the R Core Team.<br/><br/><br/><br/><br/><br/><br/><br/> "
    social = [
      ["fa-globe", "http://www.imbe.med.uni-erlangen.de/ma/S.Meyer/"],
      ["fa-info-circle", "https://orcid.org/0000-0002-1791-9449"],
      ["fa-twitter", "https://twitter.com/bastistician"],
    ]

  [[params.keynotes.members]]
    img = "mine-dogucu-400x400.jpg"
    alt = "Photo of keynote speaker Mine Dogucu"
    name = "Mine Dogucu"
    bio = "Mine Dogucu is an assistant professor of teaching in the Department of Statistics at the University of California Irvine and an incoming lecturer (teaching) in the Department of Statistical Science at University College London. She is an educator with an interest in statistics and data science education and an applied statistician with experience in educational research. She works towards the goal of making statistics and data science physically and cognitively accessible. She enjoys teaching (with) R. She is the coauthor of the book [*Bayes Rules! An Introduction to Applied Bayesian Modeling*](https://www.bayesrulesbook.com/) and the accompanying R package [`bayesrules`](https://cran.r-project.org/web/packages/bayesrules/index.html)."
    social = [
      ["fa-globe", "https://minedogucu.com"],
      ["fa-twitter", "https://twitter.com/MineDogucu"],
      ["fa-linkedin", "https://www.linkedin.com/in/minedogucu/"],
    ]



[params.organising]
  enable = false
  bg = true
  pattern= false
  photo1 = false
  photo2 = false
  # Possibility to center items
  center = true
  title = "Organisers"
  #subtitle = "Organisers"
  #description = "Organisers"

  # All organising member's pictures are stored under 'static/img/organising'.
  # Replace them with your own too.
  # Suggestion: Image size should be at least 360px*360px or the alignment might be affected.

#  [[params.organising.members]]
#    img = "faceholder.png"
#    name = "John Doe"
#    bio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non rhoncus felis, at molestie nibh. Integer ac justo cursus, convallis urna et, tempus justo. Praesent porttitor dolor sed nisl tincidunt, in malesuada risus tempor. Nulla facilisi. Vivamus elit magna, posuere vel massa ut, fermentum maximus urna. Mauris egestas dignissim viverra. Duis maximus luctus porttitor. Suspendisse laoreet velit sapien, sit amet scelerisque est consectetur et. Suspendisse tempor ipsum sit amet auctor viverra. Integer consequat felis purus, sit amet tincidunt dolor condimentum eget. Mauris dui enim, accumsan eget pharetra ut, finibus at nunc. Quisque dapibus turpis ut lectus ornare, ac congue ex."
#    social = [
#      ["fa-globe", "https://www.vumc.org/biostatistics/person/matt-shotwell-phd"], # WWW
#      ["fa-envelope", "mailto:matt.shotwell@vumc.org"], # email
#    ]

#  [[params.organising.members]]
#    img = "faceholder.png"
#    name = "Organizer Name"
#    #position = "Organizer Position"
#    social = [
#      ["fa-twitter", "#"],
#      ["fa-github", "#"],
#      ["fa-linkedin", "#"],
#      ["fa-globe", "#"], # WWW
#      ["fa-id-card", "#"] # ORCID
#    ]

[params.programming]
  enable = false
  bg = true
  pattern = false
  photo1 = false
  photo2 = false
  # Possibility to center items
  center = true
  # title = ""
  subtitle = "Program committee"
  description = "Program the thing"

  # All programming member's pictures are stored under 'static/img/programming'.
  # Replace them with your own too.
  # Suggestion: Image size should be at least 360px*360px or the alignment might be affected.
  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 1"
    position = "Programmer Position 1"
    reportsTo = "Some Person"
    livesIn = "[Munich, Germany](#some-maps-url)"
    scope = [
      "Role 1 for [thing 1](#)",
      "Role 2 for [thing 2](#)",
      "Role 3 for [thing 3](#)"
    ]
    # For bio markdown and even multiline strings are available.
    bio = """I do things,
    and stuff"""
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
    ]

  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 2"
    position = "Programmer Position 2"
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
    ]

  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 3"
    position = "Programmer Position 3"
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
    ]

  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 4"
    position = "Programmer Position 4"
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
    ]

  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 5"
    position = "Programmer Position 5"
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
    ]

  [[params.programming.members]]
    img = "faceholder.png"
    name = "Programmer Name 6"
    position = "Programmer Position 6"
    social = [
      ["fa-twitter", "#"],
      ["fa-github", "#"],
      ["fa-linkedin", "#"],
      ["fa-globe", "#"], # WWW
      ["fa-id-card", "#"] # ORCID
  ]
