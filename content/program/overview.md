+++
title = "Program"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++

# Conference Program Schedule

**useR! 2022 is an all-virtual conference**. 

All times are listed in [Central Daylight Time (CDT), UTC-05:00](https://savvytime.com/converter/cdt). On the virtual platform (Accelevents), all times will display in the local time for participants.


## Monday, June 20

|                    |                                   |
|  ---               | ---                               |
|  4:00am - 7:30am   | &nbsp; &nbsp; [Early-morning Tutorials](/program/tutorials/) |
|  9:00am - 12:30pm  | &nbsp; &nbsp; [Mid-morning Tutorials](/program/tutorials/)  |
|  2:00pm -  5:30pm  | &nbsp; &nbsp; [Afternoon Tutorials](/program/tutorials/)  |

## Tuesday, June 21

|                    |                                             |
|  ---               | ---                                         |
|  9:00am - 10:15am  | &nbsp; &nbsp; [Opening and Paula Moraga Keynote](/#keynotes) |
|  10:45am - 12:00pm | &nbsp; &nbsp; [Big Data Management](/program/talks/#session-5-big-data-management) |
|                    | &nbsp; &nbsp; [Clustering Methods](/program/talks/#session-6-clustering-methods) |
|                    | &nbsp; &nbsp; [Shiny Applications](/program/talks/#session-7-shiny-applications) |
|                    | &nbsp; &nbsp; [R in Teaching](/program/talks/#session-8-r-in-teaching) |
|                    | &nbsp; &nbsp; [Ecology and Environment](/program/talks/#session-9-ecology-and-environment) |
|                    | &nbsp; &nbsp; [Building the R Community 1](/program/talks/#session-10-building-the-r-community-1) |
|  12:00pm - 12:30pm | &nbsp; &nbsp; Speed Networking! (social event) |
|  1:00pm - 2:15pm   | &nbsp; &nbsp; [Containerization and Metaprogramming](/program/talks/#session-11-containerization-and-metaprogramming) |
|                    | &nbsp; &nbsp; [Inference Methods](/program/talks/#session-12-inference-methods) |
|                    | &nbsp; &nbsp; [Interfaces with C, C++, Rust, and V](/program/talks/#session-13-interfaces-with-c-c-rust-and-v) |
|                    | &nbsp; &nbsp; [Learning {ggplot2}](/program/talks/#session-14-learning-ggplot2) |
|                    | &nbsp; &nbsp; [Machine Learning](/program/talks/#session-15-machine-learning) |
|  2:45pm - 4:00pm   | &nbsp; &nbsp; [Amanda Cox Keynote](/#keynotes) (sponsored by Appsilon) |


## Wednesday, June 22

|                    |                                             |
|  ---               | ---                                         |
|  9:00am - 10:15am  | &nbsp; &nbsp; [afrimapr Keynote](/#keynotes) (sponsored by Oracle) |
|  10:45am - 12:30pm | &nbsp; &nbsp; [Posters + Elevator Pitches](/program/posters/) |
|  1:00pm - 2:15pm   | &nbsp; &nbsp; [Panel Discussion: Graphical User Interfaces for R](/program/talks/#session-19-panel-discussion-graphical-user-interfaces-for-r) |
|                    | &nbsp; &nbsp; [Data Visualization](/program/talks/#session-20-data-visualization) |
|                    | &nbsp; &nbsp; [Parallel Computing](/program/talks/#session-21-parallel-computing) |
|                    | &nbsp; &nbsp; [Programming and Graphics Frameworks](/program/talks/#session-22-programming-and-graphics-frameworks) |
|                    | &nbsp; &nbsp; [Publishing and Reproducibility](/program/talks/#session-23-publishing-and-reproducibility) |
|                    | &nbsp; &nbsp; [Web Frameworks](/program/talks/#session-24-web-frameworks) |
|  2:45pm - 4:00pm   | &nbsp; &nbsp; [Julia Silge Keynote](/#keynotes) |
|  4:30pm - 6:00pm   | &nbsp; &nbsp; [MemVerge Sponsored Workshop: Big Memory Computing for R (satellite event)](/participate/memverge-workshop) |

## Thursday, June 23

|                    |                                             |
|  ---               | ---                                         |
|  7:00am - 8:30am   | &nbsp; &nbsp; [MemVerge Sponsored Workshop: Big Memory Computing for R (satellite event)](/participate/memverge-workshop) |
|  9:00am - 10:30am  | &nbsp; &nbsp; [Sebastian Meyer Keynote / R Core Panel](/#keynotes) |
|  10:45am - 12:00pm | &nbsp; &nbsp; [Data Crunching with R](/program/talks/#session-27-data-crunching-with-r) |
|                    | &nbsp; &nbsp; [Package Development](/program/talks/#session-28-package-development) |
|                    | &nbsp; &nbsp; [Expanding Tidyverse](/program/talks/#session-29-expanding-tidyverse) |
|                    | &nbsp; &nbsp; [Experimental Design](/program/talks/#session-30-experimental-design) |
|                    | &nbsp; &nbsp; [Forecasting & Nowcasting](/program/talks/#session-31-forecasting-nowcasting) |
|  1:00pm - 2:15pm   | &nbsp; &nbsp; [Building the R Community 2](/program/talks/#session-32-building-the-r-community-2) |
|                    | &nbsp; &nbsp; [R GUIs](/program/talks/#session-33-r-guis) |
|                    | &nbsp; &nbsp; [Regression Models](/program/talks/#session-34-regression-models) |
|                    | &nbsp; &nbsp; [Spatial Statistics](/program/talks/#session-35-spatial-statistics) |
|                    | &nbsp; &nbsp; [Synthetic Data and Text Analysis](/program/talks/#session-36-synthetic-data-and-text-analysis) |
|                    | &nbsp; &nbsp; [Unique Applications and Methods](/program/talks/#session-37-unique-applications-and-methods) |
|  2:45pm - 4:00pm   | &nbsp; &nbsp; [Closing and Mine Dogucu Keynote](/#keynotes) |

## Thursday, June 23 - Friday, June 24

[Bug Barbecue](https://contributor.r-project.org/events/bug-bbq) (satellite event). A global contribution event organized by the [R Contribution Working Group](https://contributor.r-project.org/working-group).

|                    |                                             |
| ---                | ---                                         |
|  8:00pm - 12:00am  | &nbsp; &nbsp; Session 1 (Asia and Pacific regions) |
|  4:00am - 8:00am   | &nbsp; &nbsp; Session 2 (Europe, Middle East and Africa regions) |
|  11:00am - 3:00pm  | &nbsp; &nbsp; Session 3 (North, Central, and South America regions) |


Please visit the [Bug Barbecue](https://contributor.r-project.org/events/bug-bbq) event page for further details.
