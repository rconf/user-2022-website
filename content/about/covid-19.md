+++
title = "COVID-19"
description = "Information about COVID-19"
keywords = ["about","covid-19", "policy", "R Foundation"]
draft = true
+++

# COVID-19 Policy

In-person participants must adhere to the Vanderbilt University [COVID-19 Health and Safety Protocols](https://www.vanderbilt.edu/coronavirus/) in effect during the conference. As of November 23, 2021, all campus visitors are required to be fully vaccinated and wear a mask while indoors.
