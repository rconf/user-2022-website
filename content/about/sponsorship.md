+++
title = "Sponsorship"
description = "Become a sponsor"
keywords = ["sponsor","brochure"]
+++

# Become a sponsor for useR! 2022

Sponsorship is an important part of the useR! conferences. The sponsor packages for useR! 2022 are listed in the brochure below. If you would like to sponsor useR! 2022, please contact the organizers at [useR2022 [at] vumc.org](mailto:useR2022@vumc.org) or use the [contact form](/contact).  
![sponsorship brochure](/img/userRsponsorship-2022-02-23.png "Sponsorship Brochure")

Download the PDF [here](/pdf/useR2022-sponsorship-2022-02-23.pdf).

<br><br><br>
