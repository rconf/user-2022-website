+++
title = "Organizers"
description = "Information about the organizers"
keywords = ["about","organizers","staff", "R Foundation"]
+++

# Organizing Committee

|     |
| --- |
| Chair: [Matt Shotwell (VUMC)](https://www.vumc.org/biostatistics/person/matt-shotwell-phd) |
| [Jena Altstatt (VUMC)](https://www.vumc.org/biostatistics/person/jena-altstatt-ba) |
| [Stephen Balogun](https://twitter.com/eppydoc) |
| [Yanina Bellini Saibene (INTA/R-Ladies/LatinR/MetaDocencia)](https://yabellini.netlify.app) |
| [Audrey Carvajal (VUMC)](https://www.vumc.org/biostatistics/person/audrey-carvajal-msw-mba) |
| [Peg Duthie (VUMC)](https://www.vumc.org/biostatistics/person/peg-duthie-ma) |
| [Ben French (VUMC)](https://www.vumc.org/biostatistics/person/benjamin-french-phd) |
| [Shawn Garbett (VUMC)](https://www.vumc.org/biostatistics/person/shawn-garbett-ms) |
| [Amber Hackstadt (VUMC)](https://www.vumc.org/biostatistics/person/amber-j-hackstadt-phd) |
| [Omair A. Khan (VUMC)](https://www.vumc.org/biostatistics/person/omair-khan-mas-gstat) |
|Andrea Vargas Montero (Ixpantia) |
| [Tom Stewart (VUMC)](https://www.vumc.org/biostatistics/person/thomas-stewart-phd) |
| [Nicole Swartwood (Harvard)](https://www.hsph.harvard.edu/menzies-lab/people/nicole-swartwood/) |
| [Adithi R. Upadhya](https://adithirgis.github.io/) |
| [Janey Wang (VUMC)](https://www.vumc.org/biostatistics/person/janey-wang-ms-meng) |

The organizing team is supported by [Linux Foundation Events](https://events.linuxfoundation.org/) including Deb Giles, Ashlee Carlson, Natalie Fine, Naomi Washington, Nicole Blessing, and Tina Presley. The Linux Foundation Events team has provided support for conference registration, virtual event platform management, and accounting.

# Program Committee

|     |
| --- |
| [Luis Darcy Verde Arregoitia](https://luisdva.github.io/) |
| Louis Aslett |
| Beth Atkinson |
| Jesse Blocher |
| [Ben French](https://www.vumc.org/biostatistics/person/benjamin-french-phd) |
| [Lucy D’Agostino McGowan](https://www.lucymcgowan.com/) [@LucyStats](https://twitter.com/LucyStats) |
| [Tal Galili](https://www.r-statistics.com) [@talgalili](https://github.com/talgalili) |
| Bob Muenchen |
| Dorothea Hug Peter|
| [Riva Quiroga](https://rivaquiroga.cl/) |
|[German Rosati](https://gefero.github.io/)|
|[ Mine Çetinkaya-Rundel](https://mine-cr.com/) |
| [Yanina Bellini Saibene](https://yabellini.netlify.app) |
| Bruno Santos |
| [Natalia da Silva](http://natydasilva.com) [@pacocuak](https://twitter.com/pacocuak) |
| [Matt Shotwell](https://www.vumc.org/biostatistics/person/matt-shotwell-phd) |
| Jesse Spencer-Smith |
| [Tom Stewart](https://www.vumc.org/biostatistics/person/thomas-stewart-phd) |
| [Nicole Swartwood](https://www.hsph.harvard.edu/menzies-lab/people/nicole-swartwood/) |

# Vanderbilt Biostatistics

useR! 2022 is hosted by the [Department of Biostatistics at Vanderbilt University Medical Center](https://www.vumc.org/biostatistics/vanderbilt-department-biostatistics). Founded in 2003, the department is home to more than 45 faculty members and nearly 60 staff biostatisticians, bioinformaticians, data scientists, analysts, and application developers. Its faculty, staff, students, postdocs, and alumni have collaborated with colleagues around the world on numerous R projects in the quest to advance personalized health through cutting-edge medical research. These applications and tools include: 

 - [EHR: Electronic Health Record (EHR) Data Processing and Analysis Tool ](https://CRAN.R-project.org/package=EHR) 
 - [Hmisc: Harrell Miscellaneous](https://CRAN.R-project.org/package=Hmisc) 
 - [rccola: REDCap Crytp-O Locker for API_KEYS](https://github.com/spgarbet/rccola)
 - [rms: Regression Modeling Strategies](https://CRAN.R-project.org/package=rms) 
 - [sas7bdat: SAS Database Reader](https://cran.r-project.org/web/packages/sas7bdat/)
 - [yaml: YAML for R](https://CRAN.R-project.org/package=yaml) 

In addition to offering instruction in R through university coursework, the department also runs the Summer Institute at the Center for Quantitative Sciences, organizes programs such as the Statistical Computing Series, and maintains other resources to assist biostatisticians, clinical investigators, and other scientists and healthcare providers in fulfilling the mission of Vanderbilt University Medical Center. 

Follow the department on Twitter [@vandy_biostat](https://twitter.com/vandy_biostat), or [sign up](https://forms.office.com/Pages/ResponsePage.aspx?id=MFBX7yQU2E64PBLFM9h5q8YkaT3S-xBMhiJUg0AJgR1UQzBIWUZTVjBET1QzWkk3V1VGS0ZHNDFPNS4u) for our newsletter. 
