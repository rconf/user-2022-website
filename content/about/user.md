+++
title = "About"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++

# The useR! Conference

useR! is an annual nonprofit conference organized by R community volunteers and supported by the R Foundation. Attendees include R developers and users who are data scientists, business intelligence specialists, analysts, statisticians from academia and industry, and students. The conferences showcase applications of R software and developments in the software itself, as well as new and updated R packages that provide [boundless](http://cran.r-project.org/web/packages) additional functionality to R. Community contributions form the foundation of useR! conferences.

useR! 2022 will be all-virtual because of the uncertainty caused by the evolving COVID-19 pandemic. The conference was originally planned as a hybrid [return to Nashville, TN](https://www.r-project.org/conferences/useR-2012/), building on the successes of the global useR! 2021 all-virtual design. We are aiming to make the conference inclusive in as many ways as possible, including accessibility to underrepresented individuals and a broader international audience. We are also striving to meet the needs of people with disabilities so that they can attend and contribute to the conference.

<br><br><br>
