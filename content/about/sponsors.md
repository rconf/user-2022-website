+++
title = "Our Sponsors"
description = "Made this possible by their generous support"
keywords = ["sponsor","brochure","support","community","useR!"]
+++

# Platinum

<div class="col-md-4">
<p><img src="/img/sponsors/appsilon.png" alt="Appsilon"/></p>
</div><div class="col-md-8">
<h3 id="appsilon">Appsilon</h3>
<p><a href="https://appsilon.com/shiny/">Appsilon</a> delivers the world’s most advanced R Shiny applications, with a unique ability to rapidly develop and scale enterprise Shiny dashboards. Appsilon has created industry-leading solutions for Johnson & Johnson, Merck, and many other Global 2000 organizations. 
</p><p>
Appsilon’s global team is always working on open-source projects for the Shiny community, including <a href="https://templates.appsilon.com/">R Shiny templates</a> and <a href="https://shiny.tools/">R Shiny packages</a> like <a href="https://github.com/Appsilon/rhino">Rhino</a> and <a href="https://github.com/Appsilon/shiny.fluent">shiny.fluent</a>. Appsilon hosts the annual <a href="https://appsilon.com/2022-appsilon-shiny-conference/">Appsilon Shiny Conference</a> with support from RStudio PBC. Appsilon is committed to improving biodiversity through technology with their internally funded <a href="https://appsilon.com/data-for-good/">Data for Good project</a>.</p>
</div>

# Gold

<div class="col-md-4">
<p><br/><br/><img src="/img/sponsors/oracle.png" alt="Oracle"/></p>
</div><div class="col-md-8">
<h3>Oracle</h3>
<p>Oracle believes in the power of open-source technologies to drive innovation and has been an active member of the R Consortium since its inception in 2015. As a provider of multiple software tools and products that leverage and extend R, joining the R Consortium has been a natural way for Oracle to give back to the R community and contribute to the evolution of the R ecosystem. Oracle continues to explore ways to encourage the adoption of best practices for R package quality and security. For more information about Oracle, please visit us at <a href="https://www.oracle.com/cloud">oracle.com/cloud</a></p>
</div>

<div class="col-md-4">
<br/>
<p><img src="/img/sponsors/rconsortium.png" alt="R consortium"/></p>
</div><div class="col-md-8">
<h3 id="rconsortium">R Consortium</h3>
<p>The central mission of the R Consortium is to work with and provide support to the R Foundation and to the key organizations developing, maintaining, distributing and using R software through the identification, development and implementation of infrastructure projects. Learn more about the R Consortium at <a href="https://www.r-consortium.org/">r-consortium.org</a>.</p>
</div>

<div class="col-md-4">
<br/>
<p><img src="/img/sponsors/rstudio.png" alt="Rstudio"/></p>
</div><div class="col-md-8">
<h3>RStudio</h3>
<p>RStudio helps people understand and improve the world through data. We build tools that enable robust and reproducible data analysis through programming, paired with tools that make it easy to share insights. Our core software is open source, freely available to anyone. Our professional software equips individuals and teams to develop and share their work at scale. Learn more about RStudio <a href="https://www.rstudio.com/">rstudio.com</a>.</p>
</div>

# Silver

<div class="col-md-4">
<br/>
<p><img src="/img/sponsors/microsoft.png" alt="Microsoft Logo"/></p>
</div><div class="col-md-8">
<h3 id="microsoft">Microsoft</h3>
<p>Learn more about Microsoft at <a href="https://microsoft.com">microsoft.com</a>.<br/><br/><br/><br/><br/><br/></p>
</div>

# Bronze

<div class="col-md-4">
<br/>
<p><img width="200" src="/img/sponsors/google.png" alt="Google"/></p>
</div><div class="col-md-8">
<h3>Google</h3>
<p>Learn more about Google at <a href="https://about.google/">about.google</a>.<br/><br/><br/><br/><br/><br/></p>
</div>

<div class="col-md-4">
<br/>
<p><img width="200" src="/img/sponsors/MemVerge-logo.svg" alt="MemVerge"/></p>
</div><div class="col-md-8">
<h3 id="memverge">MemVerge</h3>
<p>Learn more about MemVerge at <a href="https://www.memverge.com/">memverge.com</a>.<br/><br/><br/><br/><br/><br/></p>
</div>

# Small Business

<div class="col-md-4">
<p><img src="/img/sponsors/cynkra.png" alt="cynkra"/></p>
</div><div class="col-md-8">
<h3 id="cynkra">cynkra</h3>
<p>Learn more about cynkra at <a href="https://www.cynkra.com">www.cynkra.com</a>.<br/><br/><br/><br/></p>
</div>

<div class="col-md-4">
<p><img src="/img/sponsors/jumping-rivers.jpg" alt="Jumping Rivers"/></p>
</div><div class="col-md-8">
<h3 id="jumping-rivers">Jumping Rivers</h3>
<p>Learn more about Jumping Rivers at <a href="https://jumpingrivers.com">jumpingrivers.com</a>.<br/><br/><br/><br/></p>
</div>

<div class="col-md-4">
<p><img src="/img/sponsors/r-bloggers.png" alt="R-bloggers"/></p>
</div><div class="col-md-8">
<h3>R-bloggers</h3>
<p>Learn more about R-bloggers at <a href="https://r-bloggers.com">r-bloggers.com</a>.<br/><br/><br/><br/></p>
</div>

<!---  --->

<div class="col-md-4">
<p><img src="/img/sponsors/opifex.png" alt="Opifex"/></p>
</div><div class="col-md-8">
<h3 id="opifex">Opifex</h3>
<p>Learn more about Opifex at <a href="https://opifex.org">opifex.org</a>.<br/><br/><br/><br/></p>

</div>
<p>

<br/><br/><br/>
