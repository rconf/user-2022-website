+++
title = "Accessibility Standards for useR! 2022 Panels"
description = "Guidelines and standards to make panels accessible."
keywords = ["Panels","Accessibility"]
+++

# UseR! 2022 Accessibility Standards for Panels

For Panels we kindly ask that organisers of a panel session: 

+ Refer to our [guidelines for slides](/participate/talks-access) if you plan to show slides in your panel or incubator. 
+ If the incubator's goal is to generate an output, please ensure that the output is accessible too. 
+ Get in touch with the accessiblity team well in advance of the conference if you have doubts about the accessibility of any tool that you plan to use during the panel or incubator. 
+ Instruct the participants of your panel to speak clearly and not too fast.

## Other reminders

- Speak as clearly as possible, looking at the camera, and try not to go too fast.
- Avoid talking at the same time of the other participants.
- Be aware of clues in the chat that signal if your pace is letting everyone follow your instructions 
- During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"

