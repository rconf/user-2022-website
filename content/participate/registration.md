+++
title = "Registration"
description = "Information about Registration and Conference Fees"
keywords = ["Fees","Registration"]
draft = false
+++

## About useR! 2022

useR! 2022 is an all-virtual conference. The useR! series is an annual nonprofit conference organized by R community volunteers and supported by the R Foundation. Attendees include R developers and users who are data scientists, business intelligence specialists, analysts, statisticians from academia and industry, and students. The conferences showcase applications of R software and developments in the software itself, as well as new and updated R packages that provide [boundless](http://cran.r-project.org/web/packages) additional functionality to R. Community contributions form the foundation of useR! conferences.

## Register

Registration is now open and will remain open throughout the conference (20-23 June, 2022). **Registration for all tutorials is closed.** 

Steps to register:

  1. Look up the registration category for your country: <a href="/participate/country-category" target="_blank">country category</a>.
  2. Review the conference and tutorial fees in your country category. Note that conference registration is required in order to register for tutorials.
  3. Click the appropriate "REGISTER" button.


## Conference Fees

Conference fees help pay for the virtual platform, honoraria for tutorial and keynote speakers, and other expenses. The fees depend on the country where you live, and whether you work in industry or academia or are a student. The _academia_ rate applies to non-profit organizations and government employees and the _student_ rate applies to retired people. Freelancers are encouraged to select the rate that best applies.

|          | High Income Country &nbsp;&nbsp; | Higher Middle Income Country &nbsp;&nbsp; | Lower Middle Income Country &nbsp;&nbsp; | Low Income Country &nbsp;&nbsp; |
|----------|---------------------|------------------------------|-----------------------------|--------------------|
| Industry &nbsp;&nbsp; | $85                 | $29                          | $12                         | waived             |
| Academia &nbsp;&nbsp; | $65                 | $22                          | $9                          | waived             |
| Student  | $45                 | $14                          | $6                          | waived             |
<br>

## Tutorial Fees

The fees listed below are for two tutorials. If you book only one tutorial, you will get a 50% discount. If you select three tutorials the fee will be 50% higher.<br>

|          | High Income Country &nbsp;&nbsp; | Higher Middle Income Country &nbsp;&nbsp; | Lower Middle Income Country &nbsp;&nbsp; | Low Income Country &nbsp;&nbsp; |
|----------|---------------------|------------------------------|-----------------------------|--------------------|
| Industry &nbsp;&nbsp; | $75                 | $22                          | $9                          | waived             |
| Academia &nbsp;&nbsp; | $55                 | $19                          | $8                          | waived             |
| Student  | $35                 | $12                          | $5                          | waived             |
<br>

### Click your country category button to proceed. 

It will take you to a Linux Foundation login page, where you will create or enter your LFID to continue the registration process.

|                  |               |
|------------------|---------------|
| **High Income** | <a href="https://cvent.me/GM4qZd" class="btn btn-primary active" style="margin: 3px 3px;padding: 5px 5px;border-radius: 5px;" target="_blank">Register (HIC)</a>&nbsp; | 
| **Higher Middle Income**&nbsp;&nbsp; | <a href="https://cvent.me/e2aq3o" class="btn btn-primary active" style="margin: 3px 3px;padding: 5px 5px;border-radius: 5px;" target="_blank">Register (HMIC)</a>&nbsp; | 
| **Lower Middle Income** | <a href="https://cvent.me/D2z7G2" class="btn btn-primary active" style="margin: 3px 3px;padding: 5px 5px;border-radius: 5px;" target="_blank">Register (LMIC)</a>&nbsp; | 
| **Low Income** | <a href="https://cvent.me/RL5AZW" class="btn btn-primary active" style="margin: 3px 3px;padding: 5px 5px;border-radius: 5px;" target="_blank">Register (LIC)</a>&nbsp; |

<br><br><br>
