+++
title = "Call for Abstracts"
description = "Speaking and Presenting"
keywords = ["presenter", "speaker", "call for abstracts",]
+++

### Abstract submissions for useR! 2022 are now closed

### Introduction 

**useR! 2022 is an all-virtual conference**. 

useR! 2022 invites R users and developers to submit abstracts to participate in this year's all-virtual conference. We are looking forward to a wide range of exciting talks, discussion panels, and poster/elevator pitch presentations. Share your work with international colleagues who are R enthusiasts, including many current and future leaders in the R community. We welcome R-related abstracts from any discipline, whether the work is computational, empirical, or theoretical in nature. Investigators at all career stages are encouraged to submit their work. 

Abstracts will be peer-reviewed. Acceptance will be based on content, available space, and overall program balance. All presenting authors for each accepted abstract will be expected to register for the conference and attend the session in which their presentation is scheduled (during June 20–23, 2022). One abstract at most will be accepted per presenter. People presenting on behalf of a community organization may additionally present their own work. 

Abstracts for the following formats are invited: 

 - Regular talk. Regular talks form the backbone of useR! conferences, with high-quality contributions from different fields. Each talk should be 15 minutes maximum in length and will be followed by a short discussion. A talk can be either live or prerecorded, but presenters of prerecorded talks should still be present for the live Q&A. 

 - Panel. Panels are meant for discussions of topics of broad interest. Typically, several experts serve as the panelists, but the audience can be involved as well. 

 - Poster with elevator pitch. This combination is designed to quickly communicate your work and facilitate discussion. Poster + elevator pitch sessions are good times to socialize! 

Details about the formats are provided below. Please read the instructions carefully. We have prepared accessibility guidelines for all formats. Please read and accept them before submitting your abstract. 

Submit your proposal by March 15, 2022. 

### Regular talks

Regular talks will be limited to 15 minutes. ~~Most regular talks should be presented in person during the conference.~~ **useR! 2022 is an all-virtual conference**. Presenters may prerecord their talk ahead of the conference so that their talk is available throughout the conference. Regular talks should be presented in English. Please consider the following guidelines when submitting an abstract for a regular talk: 

 - Talks should be new to useR! 2022.  

 - Talks should be original and directly related to the speaker’s work. Discussions or surveys of the work/packages of others may be considered for a poster + elevator pitch, but generally not for regular talks. 

 - Successful submissions for regular talks are typically backed by an active repository (in the case of a package), a technical report, a preprint, or an accepted manuscript at the time of review. If this does not apply to your work, please consider presenting a poster + elevator pitch instead. 

 - The elevator pitch format is typically a better fit for novel case studies. 

 - Your presentation should be directly related to R. General data science talks are not typically appropriate for regular talks, though some exceptions may be made. 

### Panels 
 
Panels are suitable for topics that are of interest to a broad audience. The general idea is to prepare a 60-minute topic discussion with experts/stakeholders. There can be some form of interaction with the audience (e.g., a moderated Q&A). In your abstract, give an overview of the topic and identify open questions for discussion with panelists. Please use the additional field in the form to explain whom you would like to invite and how you plan to make your panel participative and relevant for the useR! audience. 

### Poster + elevator pitch 

Posters make their return at useR! 2022! Presenting a poster at useR is an excellent way to display imaginative work and gather input from others in the R community. We welcome submissions about any work related to R; the program committee hopes to receive proposals of creative, thought-provoking content. 

Each poster presenter will be asked to upload a pdf version of their poster and an optional accompanying 5-minute elevator pitch (prerecorded audio or video). These will be hosted online for conference participants to review prior to the poster sessions, which will be hosted on the conference platform and focus on dialogue between poster presenters and attendees.  

useR! 2022 aims to be a globally accessible conference, building on the success of useR! 2021. We want useRs to have the opportunity to contribute an elevator pitch in their own language, if you feel more comfortable doing so. An English-language translation of the transcript, however, should be provided with your prerecorded elevator pitch. For the review process, please provide an English abstract. You may provide a translation of the abstract in your own language after acceptance of your contribution. 

### Community support 

useR! 2022 would like to support underrepresented groups interested in contributing to this year’s conference. Several community groups have processes in place for presenters to receive feedback on their abstracts before applying, including AfricaR, LatinR, and others. If you identify with any of these communities, please reach out to them for support and feedback!

### Submission guidelines

Send your submission via the [useR! 2022 abstract submission system](https://redcap.vanderbilt.edu/surveys/?s=DJX9LXFYKDW8HALT) by Tuesday, March 15, 2022. **Abstract submissions for useR! 2022 are now closed**


Please read the [code of conduct](/about/coc/) and the accessibility guidelines (coming soon) for your submission.

Submissions must be written in English and include: 

 - Information about you and your co-authors, along with affiliations 
 - A title 
 - A primary topic — see below for suggestions 
 - 3–5 keywords
 - An abstract of up to 250 words
 - For regular talks, we strongly encourage you to link your submission to a GitHub (GitLab, Bitbucket, or other) repo, a technical report, a preprint, or an accepted paper.
 - For posters + elevator pitches, a link to an external resource is recommended but optional. 
 - For panels, please use the additional text field to give details on the organization of your contribution (see the sections above on ‘Panels’).
 - An indication of whether or not you presented the submitted topic at a previous international R conference, and if it has been published elsewhere. 

We welcome submissions on these topics: 

 - Community and outreach 
 - Teaching R, and R in teaching 
 - Big / high-dimensional data 
 - Data mining, machine learning, deep learning and AI 
 - Databases / data management 
 - Statistical models 
 - Mathematical models 
 - Bayesian models 
 - Multivariate analysis 
 - Time series 
 - Spatial analysis 
 - Data visualization 
 - Web applications (e.g., Shiny, dashboards) 
 - Efficient programming 
 - R in production 
 - Reproducibility 
 - Interfaces with other programming languages (e.g., C++, Ruby, Python, JS) 
 - Bioinformatics / biomedical or health informatics 
 - Biostatistics / epidemiology 
 - Economics / finance / insurance 
 - Social sciences 
 - Environmental sciences 
 - Ecology 
 - Operational research and optimization 
 - R in the wild (applications of R that are unusual and rare) 
 - And more!
