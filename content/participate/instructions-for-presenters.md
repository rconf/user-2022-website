+++
title = "Instructions for Presenters and Chairs"
description = "Speaking, Presenting, and Chairing"
keywords = ["presenter", "speaker", "chair"]
+++

{{% toc %}}

# Introduction 

**useR! 2022 is an all-virtual conference**. 

Thank you for contributing to this year's all-virtual useR! 2022 conference! We are looking forward to a wide range of talks, panels, and posters, and elevator pitches from every continent (except Antarctica).

# Virtual Event Platform

Additional details about the virtual platform (Accelevents) will be shared by email with presenters (speakers and chairs) beginning the week of June 13, and with all participants the following weekend just prior to the conference. We encourage presenters to access and familiarize themselves with the virtual platform during the week of June 13.

All sessions will be automatically recorded during the conference. Recordings will be available to attendees throughout the conference. After the conference, recordings will be made publicly available on Youtube. If you prefer that recordings for your session are not shared publicly, please let us know by emailing useR2022@vumc.org.

# Tutorials 

Each tutorial session is up to 3.5 hours in duration and is led by the tutorial instructor(s). The format of the tutorial is likewise determined by the tutorial instructor. Only the tutorial instructors and registered tutorial participants will have access to tutorial session, and all tutorial participants will be able to share their video and screen. 

# Regular Talks

Each regular talk session is 1 hour and 15 minutes long and will have four talks. Sessions are grouped by topic. Each talk should be no longer than 15 minutes, with an additional 5 minutes between each talk for the speaker to answer questions from the audience and transition to the next talk. If there is time remaining at the end of all talks, there may be additional Q&A. A talk may be live or prerecorded, but presenters of prerecorded talks must be present and prepared to share their screen, "play" the recording, and participate in Q&A afterward. During the session only the session chair and speakers will have access to display their video and share their screen. Other participants may use the chat and Q&A window to ask questions. If no session chair is present at the session start time, please notify the Linux Foundation Events team in the session chat or at the help desk.

Sessions chairs will call the session to order and then hand the session off to each speaker in order, e.g., "Welcome to the session on Shiny Applications. Our first speaker is Tommy Atkins." The speaker may then share their screen (e.g., to show slides) and begin their talk. During each talk, the audience may write questions in the Q&A tab. Chairs should monitor the chat and Q&A windows to ensure that all comments and questions are on-topic and appropriate. If there are inappropriate comments or questions, the chair should notify Linux Foundation Events staff in the "backstage" window. If there is time between talks, the chair should select and verbally repeat questions from the audience (so they are captured on the session video recording) and allow the speaker to respond. The chair should then announce the next speaker. Speakers should present in the order listed on the schedule. Once the final talk is complete, the chair may use any remaining time for additional Q&A with all the session's presenters. At the conclusion of the session, the chair should thank the speakers and audience and indicate that the session has concluded. If one or more speaker is not available, the chair may end the session early if there are no remaining questions.

# Keynotes

Each keynote session (except the joint keynote/panel session on Thursday morning) is 1 hour and 15 minutes in length and the session chair will begin with announcements and a brief introduction to the speaker. Each talk should be 45-55 minutes in duration, with an additional 5-15 for the speaker to answer questions from the audience. During the presentation, only the session chair and speaker will have access to display their video and share their screen. Other participants may use the Q&A window to ask questions. The chair should monitor the chat and Q&A windows to ensure that all comments and questions are on-topic and appropriate.  If there are inappropriate comments or questions, the chair should notify Linux Foundation staff in the “backstage” window. Once the keynote talk concludes, the session chair should select and verbally repeat questions from the audience (so they are captured on the session video recording) and allow the speaker to respond. At the conclusion of the session, the chair should thank the speaker and audience and indicate that the session has concluded.

# Panels

Panel sessions are 1 hour and 15 minutes in length. Each panel will consist of several experts that will serve as the panelists, and a chair that organizes the session. However, the audience can be involved as well. Panel sessions are led by a session chair, who will call the session to order and initiate the discussion. During the panel discussion, the audience may write questions in the Q&A tab. The chair should monitor the chat and Q&A windows to ensure that all comments and questions are on-topic and appropriate.  If there are inappropriate comments or questions, the chair should notify Linux Foundation staff in the “backstage” window. Depending on how the panel is organized, the session chair may select and read aloud (so they are captured on the session video recording) questions from the audience at the appropriate time, and may direct questions to one or more panelists.

# Posters and elevator pitches

Poster presenters may upload their poster and optional elevator pitch (5 minute audio or video recording) using the upload link provided in the acceptance email. Submissions should be received no later than June 15th. Presenters should be prepared to share their poster or elevator pitch during the session, as a backup.

All posters and elevator pitches will be presented in a single session lasting 1 hour and 45 minutes (see the [program overview](/program/overview)). Presenters have been assigned to groups of four based on the topic of their presentation (see [poster groups](/program/posters)). Each poster group will have a separate 'lounge' in the virtual platform that will be accessible throughout the conference. Presenters assigned to 'Round A' should be present within their lounge area for the first 45 minutes of the poster session, and those assigned to 'Round B' should be present in their lounge for the last 45 minutes. Each lounge has a tab for asynchronous/written discussion of posters, and a Zoom-like "Live Forum" where participants may discus posters using audio-video chat.

The poster + elevator pitch session is a social event and intentionally less structured than other sessions. There are no session chairs for the poster + elevator pitch session. We recommend that poster presenters use the Live Forum functionality during their assigned time and take turns discussing their work. 
<br><br>
