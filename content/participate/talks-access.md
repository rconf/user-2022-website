+++
title = "Accessibility Standards for useR! 2022 talks"
description = "Guidelines and standards to make talks accessible."
keywords = ["Talks","Accessibility"]
+++

# UseR! 2022 Accessibility Standards for Regular Talks

For regular talks we kindly ask that prospective presenters: 

+ Prefer text-based slides platforms such as markdown or Beamer. We provide a [markdown template](https://gitlab.com/rconf/user-xaringan-theme/-/blob/master/template.Rmd) and a [xarigan useR! theme](https://gitlab.com/rconf/user-xaringan-theme)
+ Avoid transitions, animations, and complicated layouts
+ Add alt-text to images that explain completely the features in the image 
+ If you still need to present in MS PowerPoint or similar, keep the original file
+ Add speaker notes to your slides
+ Make the slides available in an accessible formats before your talk.
+ If you are giving a talk in a language different from English, provide the English transcript beforehand.
+ The slides and other materials need to be in English. You can provide a version in your native language as well.


## Other reminders

+ Speak as clearly as possible, looking at the camera, and try not to go too fast.
+ Mention slide headers or numbers when switching slides.
+ During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"
