+++
title = "How to Generate Captions - Accessibility Standards for useR! 2022"
description = "How to Generate Captions to make videos more accessible."
keywords = ["Accessibility"]
+++

# How to Generate Captions - Accessibility Standards for useR! 2022

+ Log in to YouTube and upload the video you want to add subtitles to. When uploaded, YouTube will generate an automatic subtitle using the channel's default language or the language you have specified for the video.
+ Access to [YouTube Studio](http://studio.youtube.com/).
+ Choose _Subtitles_ from from the left-hand side menu in YouTube Studio.
+ Click on the title of the video you’ll add captions to.
+ Click on the three dots at the right of the language listed and select _Download_ and choose _.srt_. This is a text file you can edit to correct any errors on the automatic captioning. You can also use this same files with [Deepl](https://www.deepl.com/translator) to do a translation of the subtitles.
+ When you finish you can click _Duplicate and Edit > Continue_ to upload the file.
+ If the langage you want is not on the list then click on _Add Language_ button and select the one you want.
+ Clik on on _Add_  and then click on _upload file_ and select the _srt_ file you edited.
+ You can check all is ok playing the video on YouTube Studio. You can also correct mistakes using YouTube Studio.



