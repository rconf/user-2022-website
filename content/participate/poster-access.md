+++
title = "Accessibility Standards for useR! 2022 Posters"
description = "Guidelines and standards to make posters accessible."
keywords = ["Technical Notes","Accessibility"]
+++

# UseR! 2022 Accessibility Standards for Poster + Elevator Pitches

For elevator pitches that accompany a poster, the general [guidelines for talks](/participate/talks-access/) apply. For posters, we kindly ask that prospective presenters: 

+ Prefer text-based platforms such as markdown or Beamer. we provide a [markdown template](https://gitlab.com/rconf/user-xaringan-theme/-/blob/master/template.Rmd) and a [xarigan useR! theme](https://gitlab.com/rconf/user-xaringan-theme)
+ Add alt-text to images and plots that explains completely the features of the image. 


## Other reminders for Q&A sessions

+ Speak as clearly as possible, looking at the camera, and try not to go too fast
+ During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"
