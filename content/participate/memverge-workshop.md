+++
title = "MemVerge Sponsored Workshop"
description = "Information about the MemVerge Sponsored Workshop"
keywords = ["MemVerge","sponsor","workshop"]
+++

## Introduction

Conference sponsor MemVerge will host a free workshop during the conference. The workshop will be a satellite event hosted outside of the useR! 2022 virtual event. Please see the description and registration information below.

## Course Title and Description

Title: Big Memory Computing for R

Big Memory Computing consists of DRAM and persistent memory that is virtualized into a lower cost pool of memory. In this crash course, R developers will how to virtualize DRAM & PMem to lower their cost of memory; how to use memory snapshots to load, save, and recover terabytes in seconds; and how to use thin clones of memory snapshots to parallelize their analytic pipelines.

## Instructor

Charlie Yu
Director of System Engineering
MemVerge

## Register

For R developers in the US - The course will be offered **Wednesday, June 22 from 2:30 to 4:00pm Pacific Time**. Attendance is free but registration is required. [Register here](https://us02web.zoom.us/webinar/register/6916554277677/WN_1CP_OgdAS7yKVHEaWk_gOg).

For R developers in Europe - The course will be offered **Thursday, June 23 from 2:00 to 4:30pm Central European Time**. Attendance is free but registration is required. [Register here](https://us02web.zoom.us/webinar/register/6016554285531/WN_9KA20opCSNyB0c4C57nHbQ).

