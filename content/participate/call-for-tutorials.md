+++
title = "Call for Tutorials"
description = "Speaking and Presenting"
keywords = ["presenter", "instructor", "call for tutorials"]
+++

### Tutorial submissions for useR! 2022 are now closed

### Introduction 

**useR! 2022 is an all-virtual conference**.

Every year useR! offers attendees tutorials to stay up to date on R-related technologies and learn new skills. The conference is for R users at any level in all sectors (including researchers, government officers, and industry representatives); we welcome tutorials that focus on the applicability of R in practical settings.

Each tutorial should be between two and three hours, including time for breaks. Tutorials should have hands-on exercises or examples for participants.  

To better serve the global audience that useR! is building, we will accept proposals for tutorials to be taught in English, Spanish or French. The proposal must be submitted in English regardless of the language you plan to teach in. 

The tutorial materials (including recordings of virtual tutorials) will be shared online after the conference, with the consent of the organizers, instructors, and attendees.  

The organization will pay USD $500 for tutorials that are selected. 

### Suggested topics

Suggested topics for this edition include the following: 

 - Environmetrics 
 - Epidemiology 
 - Big data 
 - Spatial analysis and modeling 
 - Transportation 
 - Usage of Rcpp 
 - Artificial intelligence/machine learning 
 - Non-linear statistical modeling 
 - Reproducibility and best practices 
 - Ethical web scraping 
 - Imaging and signal processing 
 - Database management 
 - Docker and RStudio 
 - Natural language processing 
 - Shiny app development and best practices 
 - Predictive modeling time series forecasting 
 - R Packages: building packages, CRAN submission and package maintenance 
 - Teaching R, and R in teaching 
 - Effective visualizations 
 - Using git with R 

### Submission guidelines 

Send your submission via the [useR! 2022 abstract submission system](https://redcap.vanderbilt.edu/surveys/?s=DJX9LXFYKDW8HALT) by Tuesday, February 15, 2022. **Tutorial submissions for useR! 2022 are now closed**

Please read the [code of conduct](/about/coc/) and the [accessibility guidelines](/participate/accessibility/) for your submission.
 
Submissions must be in English and include/indicate: 

 - Language in which the tutorial can be taught
 - The title of the tutorial
 - A brief biography of the instructors 
 - The broad topic it covers 
 - Abstract (250 words max)
 - Intended audience and prerequisites
 - A brief outline of the tutorial
 - If it exists, a link to the tutorial materials and/or webpage 


## Review Criteria 

Our program committee will carefully evaluate each submission's overall quality, research scope, and potential appeal to the R community, and check its technical content and pedagogical approach.
