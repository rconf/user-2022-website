+++
title = "Accessibility"
description = "Guidelines and standards to make useR! 2022 accessible."
keywords = ["Tutorials","Accessibility"]
author = "Andrea Sánchez-Tapia, Liz Hare, Joselyn Chávez, Silvia Canelón, Dorothea Hug Peter"
+++

# Accessibility at useR! 2022

The need for accessibility is recognized but still too often not actually enforced or considered essential to the design of a conference. The onset of the COVID-19 pandemic forced most of us to shift the ways we work and meet. Our goal at useR! 2022 is to build on the success of useR! 2021 in this area. The following observations and requests have been adapted from the original useR! 2021 guidelines by Andrea Sánchez-Tapia, Liz Hare, Joselyn Chávez, Silvia Canelón, and Dorothea Hug Peter.

## Our efforts as organizers

+ The technology team is working to ensure that the conference platform will be compatible with screen reading software and easy to navigate. 
+ The forms for abstract and tutorial submission and registration have been designed with accessibility in mind.
+ Recorded content will be captioned. We may ask participants to help with this.
+ We are striving to make all conference materials (including the schedule, abstracts, and slides) available in accessible formats before the conference.
+ We have updated these guidelines and are urging all contributors to adhere to them.

## Virtual platform

The Accelevents virtual event platform has undergone standardized auditing of 27 features/pages of the platform, including those in use for useR! 2022. This audit assessed how well these features conform with established web accessibility guidelines and standards, and evaluates which features are fully supported and which are partially supported, including the features that may have challenges with screen readers. Please contact Accelevents (info@accelevents.com) to request a copy of the accessibility conformance report, which is based on the latest version (2.4) of the Voluntary Product Accessibility Template (VPAT). Accelevents has confirmed that the platform works with most screen readers.

Human captioning will be provided for keynote lectures, with machine captioning available for all other sessions. The platform has been audited for web accessibility and confirmed to work with most screen readers.


## Accessibility guidelines
Even people who are aware that accessibility is key for inclusiveness do not necessarily know how to improve their presentations, scripts, or figures. For this reason, we ask all useR! participants to consult these guidelines, which give recommendations that are applicable to most contributions. We have also prepared short checklists that are specific to each format:

+ [Regular talks](/participate/talks-access)
+ [Poster + elevator pitches](/participate/poster-access)
+ [Panels](/participate/panels-access)
+ [Tutorials](/participate/tutorial-access)

### Slides

+ Text-based slides, such as those prepared with rmarkdown, beamer, or xaringan, are highly recommended because screen readers can read them or the text files that structure them.
+ Format your slideshow so that it can be hosted and navigated on the web. (See the "Slide availability" section for more context.)
+ If you must present slides designed in Microsoft PowerPoint or a similar program, keep and share the original file. Avoid providing only a PDF to your audience, because the semantic formatting used in the generation of PDFs (for items such as nested lists) is often indecipherable to screen readers.
+ To help presenters produce accessible slides, we provide a [markdown template](https://gitlab.com/rconf/user-xaringan-theme/-/blob/master/template.Rmd) and a [xarigan useR! theme](https://gitlab.com/rconf/user-xaringan-theme).

### Slide availability

We want to make your slides available to participants before your talk, so they can scroll through the content independently during your presentation. Place your slides on a web-based platform such as GitHub Pages, GitLab Oages, RPubs, or Netlify. Having access to the material will not only improve the conference experience for blind and low-vision participants, but also assist others in following what is being presented.


### Slide appearance and format - tips

+ Create simple slides without complicated layouts, formatting, or animations and transitions.
+ Provide alt-text for all your figures, particularly if you use image-only slides.
+ Make sure color palettes can be seen by people with color blindness.
+ Check the font size on your smallest screen. Avoid small fonts.
+ Opt for high contrast between text and backgrounds.
+ Add alt-text to your plots and figures, and describe them briefly but completely when presenting them, highlighting the essential parts of the plot according to the context of the presentation. For tutorials, include a longer explanation of how the code relates to the visual output.

### Additional tips for oral presentations and Q&A sessions

+ Speak as clearly as possible, and try not to go too fast.
+ If presenting virtually, look directly at the camera.
+ Mention slide headers or numbers when switching slides.
+ Comment briefly on what you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious to everyone, such as "As you can see" or "The image speaks for itself."

### Social media

When posting about the conference on Twitter or other platforms, please limit your use of emojis and capitalize the words inside hashtags (for example, `#WeAreUseR` instead of '#weareuser'. Include descriptive text with your images. For example, Twitter has an option to include alt-text when posting an image.


## Other considerations

At useR! 2022, we aim to welcome people from around the globe. This includes a large majority of people who are non-native speakers of English. We want to make their conference experience as comfortable as possible. Captions help participants follow presentations more easily. Regular talks and elevator pitches that accompany posters can be prerecorded in languages other than English, as long as an English-language transcript is provided for subtitles. Captions and translations are key to creating connections among as many people as possible across the conference. We provide this [short how to for creating videos captions and translations](/participate/generate-captions). 

## Additional references

 <a href="http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-conference-guide/#online" target="_blank">Accessible conference guide</a>

 <a href="https://osf.io/k3bfn/" target="_blank">Enhancing the inclusivity and accessibility of your online calls</a>

 <a href="https://www.w3.org/WAI/teach-advocate/accessible-presentations/" target="_blank">How to make your presentations accessible to all</a>

<a href="https://www.metadocencia.org/post/accesibilidad_1/" target="_blank">Cómo hacer reuniones virtuales pensadas para todas las personas (How to make virtual meetings designed for everyone)</a>

<a href="http://webaim.org/resources/contrastchecker/" target="_blank">Contrast checker</a>


Mohamed El Fodil Ihaddaden's package <a href= "https://github.com/feddelegrand7/savonliquide" target="_blank"> 'savonliquide'</a> for a contrast checker API in R

<a href="https://www.techsmith.com/blog/how-to-create-alternative-text-for-images-for-accessibility-and-seo/" target="_blank"> How to create alternative text for images for accessibility and SEO</a>

<a href="https://blog.hootsuite.com/inclusive-design-social-media/" target="_blank">Inclusive design social media</a>

<a href= "https://www.wgbh.org/foundation/ncam/guidelines/guidelines-for-describing-stem-images" target="_blank"> WGBH - guidelines for describing STEM images</a>

<a href= "http://diagramcenter.org/making-images-accessible.html" target="_blank">Diagram center (accessible graphics)</a>

<a href= "http://diagramcenter.org/accessible-math-tools-tips-and-training.html" target="_blank">Diagram center (accessible math)</a>

<a href= "https://www.csun.edu/universal-design-center/math-and-science-accessibility#A51" target= "_blank">CSUN -
math and science accessibility overview</a>

<br><br>

The guidelines above were adapted from the guidelines developed for useR! 2021. To cite those guidelines: 

```
Sánchez-Tapia, A., Hare, L., Chávez, J., Canelón, S., Hug Peter, D., 2021. useR! 2021 Accessibility guidelines. 
Retrieved from https://user2021.r-project.org/participation/accessibility/.
```

BibTeX citation

```
@misc{sanchez-tapia_user_2021,
  title = {{{useR}}! 2021 {{Accessibility}} Guidelines},
  author = {{S{\'a}nchez-Tapia}, Andrea and Hare, Liz and Ch{\'a}vez, Joselyn and Canel{\'o}n, Silvia and Hug Peter, Dorothea},
  year = {2021},
  abstract = {Guidelines and standards to make useR! 2021 accessible},
  url = {https://user2021.r-project.org/participation/accessibility/}
}
``` 
<br><br><br>
