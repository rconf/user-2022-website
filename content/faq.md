+++
title = "FAQ"
description = "Frequently asked questions"
keywords = ["FAQ","How do I","questions","what if"]
+++

## Where is useR! 2022 located?

useR! 2022 is on the Accelevents interactive virtual event platform. The platform works best in Chrome. The live conference was held from 20 through 23 June 2022, and the site will remain accessible to registered participants until 22 July 2022. 

## Will recordings of presentations be available?

Recordings of most talk sessions are now available on the conference platform, where they will be accessible until 22 July 2022 

The tech team hopes to have all tutorial recordings uploaded by 2 July. Access to these will be restricted to participants enrolled in them. 

We hope to provide access to the general public to most conference materials later this year (e.g., videos on YouTube), contingent on presenter consent. Their availability will be announced on this website and our social media platforms. Presenters may request that their recordings/materials not be circulated further. 

If slides or other materials were made available via social media, and we are aware of it, we have added a link to the presentation abstract on this website. There are no plans to publish formal conference proceedings.

## Will posters be permanently available?

Posters can be viewed on the conference platform until 22 July 2022. There are no plans for permanently archiving them. An option for individual presenters is to upload their poster to a repository that provides DOIs, such as OSF or ScienceOpen Posters.

https://osf.io 

https://www.scienceopen.com/

## How do I obtain proof of attendance?

Please submit your request through the contact form on this website. A useR administrator will send you an email certifying your participation after checking conference records.


## How do I register?

Registration for the general conference (e.g., talks and posters) was open through 23 June 2022 (the end of the live conference). 

## Can I be invoiced for my registration instead of paying for it with a credit card?

Yes! If you require an invoice for payment to be generated on your end, please contact Tina Presley at the Linux Foundation (tpresley[at]linuxfoundation[dot]org) to request one.

## How do I obtain a receipt (or proof of payment) for my registration payment?

Email registration[at]linuxfoundation[dot]org to request a copy.

## Is financial aid available? 

useR! 2022 offered diversity scholarships to members of underrepresented and historically marginalized groups presenting a poster or talk at the conference. Scholarship recipients received free registration to the general meeting and two tutorials. 

While the scholarships were reserved for presenters, we recognize that there are other useRs who may be unable to attend without financial assistance; we provided need-based registration waivers on a case-by-case basis (email useR2022[at]vumc[dot]org).  

 


## Where can I learn about accessibility at useR! 2022? 

See the [accessibility guidelines](/participate/accessibility/). 

The Accelevents virtual event platform has undergone standardized auditing of 27 features/pages of the platform, including those in use for useR! 2022. This audit assessed how well these features conform with established web accessibility guidelines and standards, and evaluates which features are fully supported and which are partially supported, including the features that may have challenges with screen readers. Please contact Accelevents (info@accelevents.com) to request a copy of the accessibility conformance report, which is based on the latest version (2.4) of the Voluntary Product Accessibility Template (VPAT). Accelevents has confirmed that the platform works with most screen readers.

Human captioning was provided for keynote lectures, with machine captioning available for all other sessions. The platform has been audited for web accessibility and confirmed to work with most screen readers.


## Is there a code of conduct for useR! 2022? 

Yes. See the [code of conduct](/about/coc/)

## How many people attended useR! 2022?

More than 1200--from every continent except Antartica! 

## When and where will the next useR! conference take place?

At the moment, there is no team in place to host useR! in 2023. The R Foundation is considering its options, including the possibility of a one-year hiatus. If your group is interested in organizing the conference in 2023 or 2024, the R Foundation Conference Committee welcomes informal enquiries at conferences[at]r-project[dot]org.

## I have a question that wasn't answered here. Whom do I contact?

Please send your query to useR[at]vumc[dot]org

